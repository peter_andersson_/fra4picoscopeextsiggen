// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

/*peter+ dummy file */ #include <atlbase.h>

using namespace std;
extern DWORD WriteExtSigGenCom(const BYTE* pWriteData, DWORD dwLen);
extern DWORD ReceiveExtSigGenCom(BYTE* pReceiveBuf, DWORD dwRecSize, DWORD dwMillisecs);

/*peter+ BEGIN*/
#define NO_CENUMERATESERIAL_USING_COMDB
#define NO_CENUMERATESERIAL_USING_CREATEFILE
#define NO_CENUMERATESERIAL_USING_QUERYDOSDEVICE
//#define NO_CENUMERATESERIAL_USING_GETDEFAULTCOMMCONFIG
#define NO_CENUMERATESERIAL_USING_SETUPAPI1
#define NO_CENUMERATESERIAL_USING_SETUPAPI2
#define NO_CENUMERATESERIAL_USING_ENUMPORTS
#define NO_CENUMERATESERIAL_USING_WMI
#define NO_CENUMERATESERIAL_USING_REGISTRY

#define CENUMERATESERIAL_USE_STL //Uncomment this line if you want to test the MFC / ATL support in CEnumerateSerial

#ifdef CENUMERATESERIAL_USE_STL
//Pull in STL support
//#include <vector>
//#include <string>
#endif

//Out of the box lets exclude CEnumerateSerial::UsingComDB for versions of Visual C 2010 or earlier as they are not shipped with the msports.h SDK header file
#if _MSC_VER <= 1600
#define NO_CENUMERATESERIAL_USING_COMDB
#endif
/*peter+ END*/
