# README #

This application builds on [FRA4PicoScope](https://bitbucket.org/hexamer/fra4picoscope/wiki/Home) created by Aaron Hexamer.

In order to extend the range of the internal signal generator, the possibility to use an external signal generator
controlled over a serial port is added. 

Details about the signal generator and the Arduino controller can be found in the [PHSNA group](https://groups.yahoo.com/neo/groups/PHSNA/info).

When the application is run, it will search for a microcontroller connected to a serial port that responds to ENQ (0x05) with ACK (0x06). 
If the search is unsuccessful the application will work as Aaron Hexamer's FRA4PicoScope 0.5.3b.

The screen capture below is an example startup window after a successful port scan.

![Image2.jpg](https://bitbucket.org/repo/xaAMrx/images/4014939105-Image2.jpg)

The screen capture below is the bode plot of the low pass filter that is on the DDS module itself.
The irregularities above 10 Mhz are probably due to getting close to the scope's BW or sampling rate limit.

![Image4.jpg](https://bitbucket.org/repo/xaAMrx/images/2110819748-Image4.jpg)

The application was developed in Visual Studio 2012 Desktop Express Edition running on Windows 7 64 bit and tested with PicoScope 2205, a "Type I" AD9850 DDS module and [SA5PMG's](https://groups.yahoo.com/neo/groups/PHSNA/files/SA5PMG_software/) version of the PHSNA software running on an Arduino Nano clone.

A windows binary is available from the project [Downloads](https://bitbucket.org/peter_andersson_/fra4picoscopeextsiggen/downloads) 

If you would like to build the application from source, download [BOOST_1_60_0](http://www.boost.org/users/history/version_1_60_0.html) and follow the instructions provided by [Aaron Hexamer](https://bitbucket.org/hexamer/fra4picoscope/wiki/Home)